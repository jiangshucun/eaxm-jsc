# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CapacityData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.CharField(max_length=64, null=True, verbose_name=b'ip', blank=True)),
                ('createtime', models.DateTimeField(verbose_name='\u4fdd\u5b58\u65f6\u95f4')),
                ('memory', models.CharField(max_length=64, verbose_name=b'\xe5\x86\x85\xe5\xad\x98')),
                ('disk', models.CharField(max_length=64, verbose_name=b'\xe7\xa3\x81\xe7\x9b\x98')),
                ('cpu', models.CharField(max_length=64, verbose_name=b'cpu')),
                ('biz_id', models.CharField(max_length=64, verbose_name=b'\xe4\xb8\x9a\xe5\x8a\xa1id')),
            ],
            options={
                'verbose_name': '\u78c1\u76d8\u5bb9\u91cf\u6570\u636e',
                'verbose_name_plural': '\u78c1\u76d8\u5bb9\u91cf\u6570\u636e',
            },
        ),
        migrations.CreateModel(
            name='CeleryTable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.CharField(max_length=64, null=True, verbose_name='ip\u5730\u5740', blank=True)),
                ('biz_id', models.CharField(max_length=64, null=True, verbose_name='\u4e1a\u52a1id', blank=True)),
                ('bkcloudid', models.CharField(max_length=64, null=True, verbose_name='cloud_id', blank=True)),
            ],
        ),
    ]
