# -*- coding: utf-8 -*-
"""
Tencent is pleased to support the open source community by making 蓝鲸智云(BlueKing) available.
Copyright (C) 2017 THL A29 Limited, a Tencent company. All rights reserved.
Licensed under the MIT License (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://opensource.org/licenses/MIT
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
"""

# from django.db import models

from django.db import models
from common.log import logger


class CapacityDataManager(models.Manager):
    @staticmethod
    def save_data(data):
        """
        保存执行结果数据
        """
        try:
            CapacityData.objects.create(
                ip=data["ip"],
                memory=data["Mem"],
                disk=data["Disk"],
                cpu=data["CPU"],
                createtime=data["createtime"],
                biz_id=data["biz_id"]
            )
            result = {'result': True, 'message': u"保存成功"}
        except Exception, e:
            logger.error(u"save_data %s" % e)
            result = {'result': False, 'message': u"保存失败, %s" % e}
        return result


class CapacityData(models.Model):
    """
    存储查询的容量数据
    """
    ip = models.CharField('ip', max_length=64, blank=True, null=True)
    createtime = models.DateTimeField(u"保存时间")
    memory = models.CharField('内存', max_length=64)
    disk = models.CharField('磁盘', max_length=64)
    cpu = models.CharField('cpu', max_length=64)
    biz_id = models.CharField('业务id', max_length=64)
    objects = CapacityDataManager()

    def __unicode__(self):
        return self.filesystem

    class Meta:
        verbose_name = u"磁盘容量数据"
        verbose_name_plural = u"磁盘容量数据"


class CeleryTable(models.Model):
    ip = models.CharField(u"ip地址", max_length=64, blank=True, null=True)
    biz_id = models.CharField(u"业务id", max_length=64, blank=True, null=True)
    bkcloudid = models.CharField(u"cloud_id", max_length=64, blank=True, null=True)
