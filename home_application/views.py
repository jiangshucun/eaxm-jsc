# -*- coding: utf-8 -*-
"""
Tencent is pleased to support the open source community by making 蓝鲸智云(BlueKing) available.
Copyright (C) 2017 THL A29 Limited, a Tencent company. All rights reserved.
Licensed under the MIT License (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://opensource.org/licenses/MIT
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
"""

import base64
import json
import time
import datetime
from common.mymako import render_mako_context, render_json
from blueking.component.shortcuts import get_client_by_request, get_client_by_user
from home_application.models import CapacityData
from home_application.models import *
from django.db.models import F, Q


def test(request):
    return render_json({"result": True, "message": "success", "data": request.user.username})


def home(request):
    """
    首页
    """
    return render_mako_context(request, '/home_application/index.html')


def home_info(request):
    """
    开发指引
    """
    return render_mako_context(request, '/home_application/home_info.html')


def contactus(request):
    """
    联系我们
    """
    return render_mako_context(request, '/home_application/contact.html')


def get_biz(request):
    biz_list = []
    client = get_client_by_user("admin")
    kwargs = {
        "fields": ['bk_biz_id', 'bk_biz_name']
    }
    resp = client.cc.search_business(**kwargs)
    if resp.get('result'):
        data = resp.get('data').get('info')
        for _d in data:
            biz_list.append({
                "biz_name": _d.get('bk_biz_name'),
                "biz_id": _d.get('bk_biz_id'),
            })
    return render_json({'result': True, 'data': biz_list})


def get_biz_server_info(request):
    biz_id = int(request.POST.get('biz_id'))
    client = get_client_by_user("admin")
    kwargs = {'condition': [
        {
            'bk_obj_id': 'biz',
            'fields': [],
            'condition': [
                {
                    'field': 'bk_biz_id',
                    'operator': '$eq',
                    'value': biz_id
                }
            ]
        }
    ]
    }
    resp = client.cc.search_host(**kwargs)
    ip_list = []
    if resp.get("result"):
        data = resp.get("data").get("info")
        for i in data:
            if i.get("host").get("bk_host_innerip") not in ip_list:
                ip_list.append({"ip": i.get("host").get("bk_host_innerip"),
                                "os_type": i.get("host").get("bk_os_name"),
                                "hostname": i.get("host").get("bk_host_name"),
                                "area": i.get("host").get("bk_cloud_id")[0].get("bk_inst_name"),
                                "bk_cloud_id": i.get("host").get("bk_cloud_id")[0].get("bk_inst_id"),
                                "biz_id": i.get("biz")[0].get("bk_biz_id"),
                                "is_celery": True if CeleryTable.objects.filter(
                                    ip=i.get("host").get("bk_host_innerip")) else False})
    result = {'result': True, 'data': ip_list}
    return render_json(result)


def get_servers_info(request):
    ips = request.POST.get('ips')
    ips = ips.split("\n")
    ip_list = []
    for ip in ips:
        client = get_client_by_user("admin")
        kwargs = {'condition': [
            {
                'bk_obj_id': 'host',
                'fields': [],
                'condition': [
                    {
                        'field': 'bk_host_innerip',
                        'operator': '$eq',
                        'value': ip
                    }
                ]
            },
            {
                "bk_obj_id": "module",
                "fields": [],
                "condition": []
            },
            {
                "bk_obj_id": "set",
                "fields": [],
                "condition": []
            },
            {
                "bk_obj_id": "biz",
                "fields": [],
                "condition": []
            }
        ]
        }
        resp = client.cc.search_host(**kwargs)
        if resp.get("result"):
            data = resp.get("data").get("info")
            for i in data:
                if i.get("host").get("bk_host_innerip") not in ip_list:
                    ip_list.append({"ip": i.get("host").get("bk_host_innerip"),
                                    "os_type": i.get("host").get("bk_os_name"),
                                    "hostname": i.get("host").get("bk_host_name"),
                                    "area": i.get("host").get("bk_cloud_id")[0].get("bk_inst_name"),
                                    "bk_cloud_id": i.get("host").get("bk_cloud_id")[0].get("bk_inst_id"),
                                    "biz_id": i.get("biz")[0].get("bk_biz_id"),
                                    "is_celery": True if CeleryTable.objects.filter(
                                        ip=i.get("host").get("bk_host_innerip")) else False})
    result = {'result': True, 'data': ip_list}
    return render_json(result)


# 获取使用率
def get_status_info(request):
    ip = request.POST.get('ip')
    biz_id = request.POST.get('biz_id')
    bkcloudid = request.POST.get('bkcloudid')
    client = get_client_by_user("admin")
    job_instance_id = _fast_execute_script(client, biz_id, ip, bkcloudid)
    if job_instance_id == "-1":
        return render_json({'result': False})
    time.sleep(2)
    return_data = _get_job_instance_log(client, biz_id, job_instance_id)
    if return_data:
        result = {'result': True, 'data': return_data[0]}
    else:
        result = {'result': False}
    return render_json(result)


def _get_job_instance_log(client, biz_id, job_instance_id, count=0):
    return_data = []
    try:
        kwargs = {
            'bk_biz_id': biz_id,
            'job_instance_id': job_instance_id,
        }
        resp = client.job.get_job_instance_log(**kwargs)
        if resp.get('result'):
            data = resp.get('data')
            for _d in data:
                if _d.get('is_finished'):
                    ip_logs = _d['step_results'][0].get('ip_logs')
                    for _aip in ip_logs:
                        logs = _aip.get('log_content')
                        log = logs.strip("\n").split("|")
                        return_data.append({
                            'ip': _aip.get('ip'),
                            'Mem': log[1],
                            'Disk': log[2],
                            'CPU': log[3],
                            'createtime': log[0],
                            'biz_id': biz_id
                        })
                    return return_data
                else:
                    if count < 5:
                        count += 1
                        time.sleep(2)
                        return _get_job_instance_log(client, biz_id, job_instance_id, count)
                    else:
                        return return_data
        else:
            if count < 5:
                count += 1
                time.sleep(2)
                return _get_job_instance_log(client, biz_id, job_instance_id, count)
            else:
                return return_data
    except Exception as e:
        logger.info(e)
        return return_data


def _fast_execute_script(client, biz_id, ip, bkcloudid):
    """
    执行Job作业
    """
    script_content = '''#!/bin/bash
MEMORY=$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')
DISK=$(df -h | awk '$NF=="/"{printf "%s", $5}')
CPU=$(top -bn1 | grep load | awk '{printf "%.2f%%", $(NF-2)}')
DATE=$(date "+%Y-%m-%d %H:%M:%S")
echo -e "$DATE|$MEMORY|$DISK|$CPU"'''
    kwargs = {
        "account": "root",
        "bk_biz_id": biz_id,
        "script_type": 1,
        "script_content": base64.b64encode(script_content),
        "ip_list": [
            {
                "bk_cloud_id": bkcloudid,
                "ip": ip
            }]
    }
    resp = client.job.fast_execute_script(**kwargs)
    if resp.get('result'):
        job_instance_id = resp.get('data').get('job_instance_id')
    else:
        job_instance_id = -1
    return job_instance_id


def add_check_list(request):
    ip = request.POST.get("ip")
    biz_id = request.POST.get("biz_id")
    bkcloudid = request.POST.get("bkcloudid")
    if CeleryTable.objects.filter(ip=ip):
        return render_json({'result': False})
    else:
        CeleryTable.objects.create(ip=ip, biz_id=biz_id, bkcloudid=bkcloudid)
    return render_json({'result': True})


def delete_check_list(request):
    ip = request.POST.get("ip")
    CeleryTable.objects.filter(ip=ip).delete()
    return render_json({'result': True})


def get_photo_data(request):
    biz_id = request.POST.get("biz_id")
    ips = request.POST.get("ips")
    now = datetime.datetime.now()
    start = now - datetime.timedelta(hours=1)
    if not (ips or biz_id):
        server_list = CapacityData.objects.filter(createtime__range=(start, now)).values("ip").distinct()
    elif biz_id:
        server_list = CapacityData.objects.filter(biz_id=str(biz_id), createtime__range=(start, now)).values(
            "ip").distinct()
    else:
        ips = ips.split("\n")
        server_list = CapacityData.objects.filter(ip__in=ips, createtime__range=(start, now)).values("ip").distinct()
    return_data = []
    logger.info(server_list)
    for a in server_list:
        id_ip = a.get("ip").replace(".", "_")
        categories = []
        series = []
        series_mem_data = []
        series_dick_data = []
        series_cpu_data = []
        ser_list_ = CapacityData.objects.filter(ip=a.get("ip"), createtime__range=(start, now))
        for i in ser_list_:
            categories.append(i.createtime.strftime('%Y-%m-%d %H:%M:%S'))
            series_mem_data.append(float(i.memory.strip("%")))
            series_cpu_data.append(float(i.cpu.strip("%")))
            series_dick_data.append(float(i.disk.strip("%")))
        series.append({
            "color": "red",
            "name": "内存使用占比符号（%）",
            "data": series_mem_data
        })
        series.append({
            "color": "blue",
            "name": "CPU使用占比符号（%）",
            "data": series_cpu_data
        })
        series.append({
            "color": "green",
            "name": "磁盘使用占比符号（%）",
            "data": series_dick_data
        })
        return_data.append({"id": id_ip, "series": series, "categories": categories})
    # return_data = [{
    #     "id": "123_2_3",
    #     "series": [{
    #         "color": "black",
    #         "name": "dick",
    #         "data": [100, 30, 120, 150, 125, 76, 135, 162, 32, 20, 6, 3]
    #     }, {
    #         "color": "red",
    #         "name": "men",
    #         "data": [200, 180, 190, 150, 125, 76, 133, 122, 32, 20, 6, 3]
    #     }],
    #     "categories": ["07:10", "07:10", "07:10", "07:10", "07:10", "07:10", "07:10", "07:10", "07:10", "07:10",
    #                    "07:10", "07:10"]
    # }
    # ]
    return render_json({"result": True, "data": return_data})
